const header = document.querySelector('header')
const main = document.querySelector('main')
const footer = document.querySelector('footer')
const aside = document.querySelector('aside')


/* < выезд div-ов */
const isVisible = (element) => {
 const windowHeight = document.documentElement.clientHeight
 //
 const element_ = element.getBoundingClientRect()
 const elementTop = element_.top + (windowHeight / 100 * 20)
 const elementBottom = element_.bottom - (windowHeight / 100 * 20)

 const topVisible = elementTop > 0 && elementTop < windowHeight
 const bottomVisible = elementBottom < windowHeight && elementBottom > 0

 return topVisible || bottomVisible
}

const handlerWindowResize = () => {
 const div = header.querySelector('section > div');
 div.style.marginBottom = `calc(((100vh - ${div.offsetHeight}px) / 2) + (${header.offsetHeight}px - 100vh))`//; console.log(div.style.marginTop)
 setTimeout(() => {
  div.querySelector('button').style.zIndex = 3
 }, 1600)
}

const handlerWindowScroll = () => {
 if(isVisible(header) && !header.querySelector('section > div').hasAttribute('style')){
  handlerWindowResize()
 }

 if(isVisible(main) && !main.querySelector('section > div').hasAttribute('style')){
  main.querySelector('section > div').style.marginBottom = '0px'
  setTimeout(() => {
   main.querySelector('section > div > div').style.zIndex = 3
  }, 1600)
 }

 if(isVisible(footer) && !footer.querySelector('section > div').hasAttribute('style')){
  footer.querySelector('section > div').style.marginBottom = '0px'
  setTimeout(() => {
   footer.querySelector('section > div > div').style.zIndex = 3
  }, 1600)
 }
}

//window.onload = handlerWindowScroll
document.addEventListener('DOMContentLoaded', handlerWindowScroll)
window.addEventListener('scroll', handlerWindowScroll)
window.addEventListener('resize', handlerWindowResize, true)
/* < выезд div-ов */

/* < footer */
const faq_items = footer.querySelectorAll('.faq_item')
Array.from(faq_items).forEach((f) => {
 f.addEventListener('click', () => {
  Array.from(faq_items).forEach((f) => {
   f.querySelector('.details').removeAttribute('style')
  })
  f.querySelector('.details').style.height = '80px'
 })
})
/* > footer */

/* < popUp */
const switchPopUp = () => {
 if(!aside.hasAttribute('hidden')){
  document.querySelector('aside > div').style.opacity = 0
  setTimeout(() => {
   aside.style.opacity = 0
   setTimeout(() => {
    aside.hidden = true
   }, 400)
  }, 200)
 } else{
  aside.hidden = false
  setTimeout(() => {
   aside.style.opacity = 1
   setTimeout(() => {
    document.querySelector('aside > div').style.opacity = 1
   }, 200)
  }, 400)
 }
}

const cFonds = main.querySelectorAll('div > a')
Array.from(cFonds).forEach((fond) => {
 fond.addEventListener('click', (e) => {
  e.preventDefault()

  const t = (e.target.tagName === 'IMG') ? e.target.parentNode : e.target //; console.log('t', t.tagName);
  //
  const a_A = t.querySelector('img').getAttribute('src').split('/')
  const a_fileName = a_A[a_A.length - 1].split('.')

  document.querySelector('aside > div').style.backgroundImage = `url(/assets/images/main/popUp/bg/${a_fileName[0]}.jpg)`
  aside.querySelector('aside .buttons > a').setAttribute('href', `//${t.dataset.url}`)
  aside.querySelector('#popUp_detail > a').setAttribute('href', `//${t.dataset.url}`)
  aside.querySelector('#popUp_detail > a > img').setAttribute('alt', t.querySelector('img').getAttribute('alt'))
  aside.querySelector('#popUp_detail > a > img').setAttribute('src', t.querySelector('img').getAttribute('src'))
  aside.querySelector('output').innerHTML = t.dataset.description

  switchPopUp()
  aside.querySelector('span').addEventListener('click', switchPopUp)
 })
})
/* > popUp */

/* < burger-cross */
header.querySelector('#burger-cross').addEventListener('click', (e) => {
 header.querySelector('#burger-cross').classList.toggle('open')
 document.querySelector('#mobileMenu').classList.toggle('active')
})

document.addEventListener('DOMContentLoaded', () => {
 document.querySelector('#mobileMenu > menu').innerHTML = header.querySelector('menu').innerHTML
})
/* > burger-cross */

/* < */
const c_b = document.querySelectorAll('button[data-href], .button[data-href]')
Array.from(c_b).forEach((b) => {
 b.addEventListener('click', (e) => { //console.log('e.target', e.target)
  e.preventDefault()

  const b = (e.target.tagName !== 'button') ? e.target.closest('button, .button') : e.target

  window.open(b.dataset.href)
 })
})
/* > */